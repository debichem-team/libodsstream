libodsstream (0.9.7-1) unstable; urgency=low

  * New upstream version.

  * Some cleanup, with git repos removal of files that are generated.

  * This package does not need t64 transition processing, although it
    reverse-depends on affected libquazip1-qt6 (no changes needed
    because the required package is a -dev package).

 -- Filippo Rusconi <lopippo@debian.org>  Wed, 07 Feb 2024 14:17:08 +0100

libodsstream (0.9.6-1~bookworm+1) bookworm; urgency=medium

  * catch up with the official debian package version

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 20 Dec 2023 12:26:50 +0100

libodsstream (0.9.6-1) unstable; urgency=low

  * New upstream version fixing bug.

 -- Filippo Rusconi <lopippo@debian.org>  Mon, 04 Dec 2023 22:01:13 +0100

libodsstream (0.9.5-1~bookworm+1) bookworm; urgency=medium

  * fix a problem reading ods cell containing annotations

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 01 Dec 2023 15:16:09 +0100

libodsstream (0.9.4-2) unstable; urgency=low

  * Fix the hardening flags.

 -- Filippo Rusconi <lopippo@debian.org>  Fri, 24 Nov 2023 14:11:48 +0100

libodsstream (0.9.4-1) unstable; urgency=low

  * New version to fix some glitches introduced in the previous version.

 -- Filippo Rusconi <lopippo@debian.org>  Sat, 18 Nov 2023 19:19:53 +0100

libodsstream (0.9.3-1) unstable; urgency=low

  * New upstream version fixing the catch2 header problem. Since we build for
    stable also, we had to fix the problem with an #if #else include statement
    for the Catch2 header file. Thanks to Andreas Tille for investigating the
    problem (https://salsa.debian.org/tille). The bug that was closed by his
    tentative fix was bug #1054707.

  * Debhelper compat to version = 13.

  * Standards-Version: 4.6.2

  Andreas Tille:
    libodsstream (0.9.1-3) unstable; urgency=medium

    * Team upload.
    * cme fix dpkg-control
    * Port to catch2 v3 (thanks for the hint to Sune Vuorela)
      Closed: #1054707

  -- Andreas Tille <tille@debian.org>  Tue, 07 Nov 2023 15:29:21 +0100

 -- Filippo Rusconi <lopippo@debian.org>  Sat, 18 Nov 2023 14:31:46 +0100


libodsstream (0.9.2-2~bookworm+2) bookworm; urgency=medium

  * bookworm package

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 16 Jun 2023 18:59:15 +0200

libodsstream (0.9.2-1~bullseye+1) bullseye; urgency=medium

  * switch to Qt6 for Bullseye

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 17 Mar 2023 11:35:55 +0100

libodsstream (0.9.1-1~bullseye+1) bullseye; urgency=medium

  * bullseye backport.

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 19 Dec 2022 11:40:10 +0100

libodsstream (0.9.1-1) unstable; urgency=low

  * Rework the CMake-based Config.cmake generation process to stick to more
    modern approaches.

  * Add libquazip1-qt6-dev and other development packages as
    libodsstream-dev's dependency.

  * dh_auto_test added in d/rules.

 -- Filippo Rusconi <lopippo@debian.org>  Tue, 13 Dec 2022 22:48:47 +0100

libodsstream (0.9.0-2) UNRELEASED; urgency=medium

  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on dh-exec.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 02 Nov 2022 08:50:29 -0000

libodsstream (0.9.0-1) unstable; urgency=low

  * Port of the project to Qt6.

  * Standards-Version: 4.6.1 (routine update).

 -- Filippo Rusconi <lopippo@debian.org>  Tue, 13 Sep 2022 21:09:28 +0200

libodsstream (0.8.5-1~bullseye+1) bullseye; urgency=medium

  * bullseye backport

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 31 Oct 2022 09:10:37 +0100

libodsstream (0.8.5-1) unstable; urgency=low

  * New version to fix distribution.

 -- Filippo Rusconi <lopippo@debian.org>  Fri, 28 Oct 2022 03:52:21 +0200

libodsstream (0.8.4-1) bullseye; urgency=low

  * New upstream version.

  * Standards-Version: 4.6.1 (routine update)

 -- Filippo Rusconi <lopippo@debian.org>  Fri, 28 Oct 2022 02:26:21 +0200

libodsstream (0.8.3-1~bullseye+1) bullseye; urgency=medium

  * bug fix on ZipTsvOutputStream

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 05 Oct 2022 10:38:55 +0200

libodsstream (0.8.2-1) unstable; urgency=low

  * Debian packaging: fix the erroneous distribution (see stanza below).

 -- Filippo Rusconi <lopippo@debian.org>  Mon, 11 Jul 2022 11:12:31 +0200

libodsstream (0.8.1-1) bullseye; urgency=low

  * Debian packaging.

 -- Filippo Rusconi <lopippo@debian.org>  Fri, 01 Jul 2022 10:06:37 +0200

libodsstream (0.8.0-1) bullseye; urgency=medium

  * fix for integer writing in TSV output
  * new Zipped file tsv output ZipTsvOutputStream

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 28 Jun 2022 11:06:54 +0200

libodsstream (0.7.11-1) unstable; urgency=low

  * New upstream release.

  * d/control: Remove section for libodsstream0 that duplicates source's
    section.

  * d/control: Remove the -qt5-named transitional packages.

  * d/control: Remove Langella from uploaders field.

 -- Filippo Rusconi <lopippo@debian.org>  Tue, 05 Jan 2021 11:27:07 +0100

libodsstream (0.7.10-1~buster+1) buster; urgency=medium

  * better tsv API to use flush on output streams

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 16 Dec 2020 11:37:28 +0100

libodsstream (0.7.9-1) unstable; urgency=low

  * New upstream version that fixes bug on 32-bit platforms (Closes: #959539).

  * Improvements to the packaging with use of dh-exec for the d/links files.

 -- Filippo Rusconi <lopippo@debian.org>  Thu, 18 Jun 2020 15:10:11 +0200

libodsstream (0.7.8-4) buster; urgency=medium

  * fix for Debian Bug report logs - #959539

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 15 Jun 2020 08:21:19 +0200

libodsstream (0.7.8-3) buster; urgency=medium

  * buster port

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 18 May 2020 10:03:21 +0200

libodsstream (0.7.8-2) unstable; urgency=low

  * Merge in the modifications from Andreas Tille (thank you!)

  [ Andreas Tille ]
  * debhelper-compat 12 (routine-update)
  * cme fix dpkg-control
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Remove trailing whitespace in debian/control (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Remove trailing whitespace in debian/rules (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Trim trailing whitespace.

 -- Filippo Rusconi <lopippo@debian.org>  Tue, 05 May 2020 18:33:18 +0200

libodsstream (0.7.8-1) unstable; urgency=low

  * Conversion of all the files to UNIX format (some had a DOS format);

  * Removal of all the warnings generated by GCC (mainly using the
    [[maybe_unused]] directive).

  * Ensure that the naming of the lib in the CMake dialect is according to the
    scheme: OdsStream and not ODSSTREAM...

  * Ensure that gbp import-orig is used to store the upstream tarball in the
    git repos (closes: #959637). Warm thanks to Andras Tille for spotting the
    problem and reporting it <andreas@an3as.eu>.

 -- Filippo Rusconi <lopippo@debian.org>  Thu, 30 Apr 2020 16:19:46 +0200


libodsstream (0.7.7-1) unstable; urgency=low

  * New upstream version with a fix for the libquazip5-finding
    CMake module.

  * Automated the version numbering in the d/*.install files with
    CMake config files.

 -- Filippo Rusconi <lopippo@debian.org>  Mon, 27 Apr 2020 10:18:22 +0200

libodsstream (0.7.6-1) unstable; urgency=low

  * New upstream starting the implementation of MultiArch in
    the CMake build system.

  * Fixes to the packaging with respect to MultiArch.

  * Introduce an unversioned Conflict: for package replacement as
    it seemed to be needed.

 -- Filippo Rusconi <lopippo@debian.org>  Thu, 23 Apr 2020 21:37:40 +0200

libodsstream (0.7.5-1) unstable; urgency=low

  * New upstream version.

  * Small fixes to the Debian packaging bits.

 -- Filippo Rusconi <lopippo@debian.org>  Tue, 21 Apr 2020 14:57:06 +0200

libodsstream (0.7.4-2) buster; urgency=medium

  * merge with mia repository

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 08 Apr 2020 18:39:45 +0200

libodsstream (0.7.4-1) buster; urgency=medium

  * new tests for better reliability
  * modern exception handler

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 08 Apr 2020 18:30:56 +0200

libodsstream (0.7.3-3ubuntu1) bionic; urgency=medium

  * bionic beaver version

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 10 Jan 2020 17:56:30 +0000

libodsstream (0.7.3-3) buster; urgency=medium

  * better cmake config

 -- Olivier Langella <olivier.langella@u-psud.fr>  Thu, 19 Dec 2019 21:45:51 +0100

libodsstream (0.7.3-2) buster; urgency=medium

  * write std::size_t, new cmake targets

 -- Olivier Langella <olivier.langella@u-psud.fr>  Thu, 19 Dec 2019 21:14:59 +0100

libodsstream (0.7.3-1) buster; urgency=medium

  * new cmake syntax, slight API changes

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 13 Nov 2019 16:41:32 +0100

libodsstream (0.7.2-3) buster; urgency=medium

  * buster package

 -- Olivier Langella <olivier.langella@u-psud.fr>  Thu, 12 Sep 2019 15:49:35 +0200

libodsstream (0.7.2-1) buster; urgency=medium

  * first buster package

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 05 Aug 2019 13:02:35 +0200

libodsstream (0.7.1-1) stretch; urgency=medium

  * new QtableWriter object

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 18 Mar 2019 10:52:14 +0100

libodsstream (0.7.0-2) stretch; urgency=medium

  * new API to read text files (TSV, CSV...)

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 11 Dec 2018 10:49:23 +0100

libodsstream (0.6.3-1) stretch; urgency=medium

  * conformance with ODF validator

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sat, 15 Sep 2018 22:19:19 +0200

libodsstream (0.6.2-3) stretch; urgency=medium

  * ODS reader problem is fixed

 -- Olivier Langella <olivier.langella@u-psud.fr>  Thu, 16 Aug 2018 10:17:04 +0200

libodsstream (0.6.2-2) stretch; urgency=medium

  * read anchor text value (bug fix)

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 14 Aug 2018 15:43:22 +0200

libodsstream (0.6.2-1) stretch; urgency=medium

  * read anchor text value

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 14 Aug 2018 14:23:10 +0200

libodsstream (0.6.1-1) stretch; urgency=medium

  * bug fix on odscolorscale

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 15 May 2018 12:52:23 +0200

libodsstream (0.6.0-2) stretch; urgency=medium

  * API fix

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 30 Apr 2018 17:06:43 +0200

libodsstream (0.6.0-1) stretch; urgency=medium

  * code refactoring, new API

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 30 Apr 2018 13:32:31 +0200

libodsstream (0.5.2-2) stretch; urgency=medium

  * fix documentation install problem

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sat, 14 Apr 2018 17:15:52 +0200

libodsstream (0.5.2-1) stretch; urgency=medium

  * ODS color scale fixed for sheetnames containing spaces

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 09 Apr 2018 13:42:30 +0200

libodsstream (0.5.1-1) stretch; urgency=medium

  * new color scale conditional format feature

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sun, 11 Mar 2018 08:02:15 +0100

libodsstream (0.4.13-3) stretch; urgency=medium

  * fix rename problem using virtual package

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 12 Feb 2018 10:57:44 +0100

libodsstream (0.4.13-2) stretch; urgency=medium

  * add a conflict on the old binary package

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 12 Feb 2018 09:23:50 +0100

libodsstream (0.4.13-1) stretch; urgency=medium

  * newer debian configuration files

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sun, 11 Feb 2018 16:01:22 +0100

libodsstream (0.4.12-1) stretch; urgency=medium

  [ Olivier Langella ]
  * first attempt to build an official debian package

  [ Andreas Tille ]
  * Add watch file
  * Set Debichem team as maintainer
  * Add Vcs fields
  * cme fix dpkg-control
  * source/format: 3.0 (quilt)
  * remove dbgsym packages
  * d/rules: short dh
  * Add missing Build-Depends: cmake
  * debhelper 10
  * Standards-Version: 4.1.2

  [ Olivier Langella ]
  * newer debian configuration files

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 14 Nov 2017 14:53:53 +0100

libodsstream (0.4.10-1) stretch; urgency=medium

  * bug fixed writing URL cells

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sun, 13 Aug 2017 14:34:21 +0200

libodsstream (0.4.9-1) stretch; urgency=medium

  * virtual function to write sheetname

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 20 Jun 2017 21:38:56 +0200

libodsstream (0.4.8-2) stretch; urgency=medium

  * first stetch package

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 20 Jun 2017 17:15:08 +0200

libodsstream (0.4.8-1) jessie; urgency=medium

  * new cell format for percentage

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Tue, 23 May 2017 22:01:00 +0200

libodsstream (0.4.7-2) jessie; urgency=medium

  * ends tsv files with EOL

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Tue, 17 Jan 2017 15:49:21 +0100

libodsstream (0.4.7-1) jessie; urgency=medium

  * ods2tsv package

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Fri, 13 Jan 2017 16:15:20 +0100

libodsstream (0.4.6-2) stretch; urgency=medium

  * stretch package

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sat, 17 Dec 2016 07:47:07 +0100

libodsstream (0.4.6-1) jessie; urgency=medium

  * tabulation bug fixed, thanks to T. Balliau

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Mon, 12 Dec 2016 16:14:39 +0100

libodsstream (0.4.5-3) stretch; urgency=medium

  * cmake 3.7

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Sun, 20 Nov 2016 16:12:52 +0100

libodsstream (0.4.5-2) stretch; urgency=medium

  * install cmake module

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Sat, 03 Sep 2016 14:07:28 +0200

libodsstream (0.4.5-1) stretch; urgency=medium

  * package for stretch

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Thu, 25 Aug 2016 21:53:45 +0200

libodsstream (0.4.4-1) jessie; urgency=medium

  * write annotations in cells

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Fri, 19 Aug 2016 15:29:35 +0200

libodsstream (0.4.3-1) jessie; urgency=medium

  * better flush and close

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Thu, 21 Apr 2016 19:12:02 +0200

libodsstream (0.4.2-1) jessie; urgency=medium

  * virtual destructor for calc writer interface

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Tue, 19 Apr 2016 22:09:16 +0200

libodsstream (0.4.1-1) jessie; urgency=medium

  * API to write ODS directly in file

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Tue, 12 Apr 2016 21:07:25 +0200

libodsstream (0.4.0-1) jessie; urgency=medium

  * switch licence from GPLv3 to LGPLv3

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Fri, 16 Oct 2015 14:19:06 +0200

libodsstream (0.3.0-3) jessie; urgency=medium

  * SOVERSION number fixed

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Thu, 20 Aug 2015 21:36:18 +0200

libodsstream (0.3.0-2) jessie; urgency=medium

  * library informations added in debian package (symbols, shlibs)

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Thu, 20 Aug 2015 21:23:15 +0200

libodsstream (0.3.0-1) jessie; urgency=medium

  * first cell style support API

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Mon, 17 Aug 2015 17:04:04 +0200

libodsstream (0.2.1-1) jessie; urgency=medium

  * TSV output stream

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Mon, 13 Jul 2015 08:57:58 +0200

libodsstream (0.2.0-2) jessie; urgency=medium

  * make calc writer interface public

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Wed, 24 Jun 2015 22:28:32 +0200

libodsstream (0.2.0-1) jessie; urgency=medium

  * write tsv directory

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Wed, 24 Jun 2015 19:51:01 +0200

libodsstream (0.1.3-1) jessie; urgency=medium

  * write empty cell

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Tue, 16 Jun 2015 21:00:21 +0200

libodsstream (0.1.2-2) jessie; urgency=medium

  * endif bug fixed

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Fri, 12 Jun 2015 10:37:27 +0200

libodsstream (0.1.2-1) jessie; urgency=medium

  * double compilation qt4 and qt5

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Fri, 12 Jun 2015 10:16:02 +0200

libodsstream (0.1.1-2) jessie; urgency=medium

  * writeLine bug fixed

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Wed, 10 Jun 2015 13:27:22 +0200

libodsstream (0.1.1-1) jessie; urgency=medium

  * new version using Qt5

 -- Olivier Langella <olivier.langella@moulon.inra.fr>  Wed, 10 Jun 2015 07:56:44 +0200

libodsstream (0.0.1-1) wheezy; urgency=low

  * first debian package

 -- Olivier Langella <olivier.langella@moulon.inra.fr>  Sat, 6 Jul 2013 06:18:51 +0200
